
SHELL := /bin/bash

-include .env
export

.PHONY: init
init:
	mkdir -p data train eval
	poetry install --verbose -vv
	git config commit.template $(pwd)/.gitmessage
	poetry run dvc init --subdir
	poetry run dvc remote add -d s3pond s3://dvc-s3-remote/partial_labelling
	poetry run dvc remote modify s3pond credentialpath ${HOME}/.aws/credentials
	poetry run dvc remote modify s3pond region ap-southeast-2
	poetry run dvc remote modify s3pond profile silverbrane



.PHONY: tboard
tboard:
	tensorboard --logdir=train:train/artifacts/tf_logs/,eval:eval/artifacts/

.PHONY: jupyter
jupyter:
	jupyter lab --allow-root --no-browser .
