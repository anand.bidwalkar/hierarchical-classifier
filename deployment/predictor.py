import numpy as np
from model import (
    MMDetectionDetector,
)
from highlighter_client import graphql as gql
from highlighter_client.deployment.cortex import CortexModelDeployment
from pathlib import Path

class PythonPredictor(CortexModelDeployment):
    """
    ToDo Move to MMPOND or HCP or something

    """
    def __init__(self, config):
        super().__init__(config)

        # setup models
        # TODO Make this configuralbe, e.g. using pipeline config
        with self.context:
            print("Building detector")
            if not Path("model_stuff.tar.gz").exists():
                gql.export_model_files(training_run_id=config["training_run_id"])

        tars = list(Path(".").glob("*.tar.gz"))
        assert len(tars) == 1, f'Expected exactly 1 tar.gz file, found: {tars}'

        self.model = MMDetectionDetector.from_storage(
            str(tars[0]),
            conf_threshold=self.threshold if self.threshold is not None else 0.7,
        )

    def predict_single_image(self, image):
        result = self.model(np.array(image)[None])
        return result
