import numpy as np
import torch
from mmpond.inference.hlml_format import get_inference_model
from mmpond.inference.basic import DetectionModel
import json
import mmcv
import torchvision as tv
from pathlib import Path
from shapely import geometry as gm
from hl_ml.shapes import MultiPoly
import tarfile
import tempfile
from typing import Optional, List, Dict, Any
from highlighter_client.images.utils import write_image

class MMDetectionDetector:
    def __init__(self, model: DetectionModel):
        self.model = model

    @torch.no_grad()
    def __call__(self, images: np.ndarray):
        assert len(images) == 1
        boxes = self.model(images)[0]
        return boxes.to_hl_suggestion_format()

    @classmethod
    def from_storage(cls, archive_file, conf_threshold=0.5):
        """TODO. Move to MMPOND
        """
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpdir = Path(tmpdir)
            with tarfile.open(archive_file) as storage:
                storage.extractall(tmpdir)
            (cfg_file,) = tmpdir.glob("*.py")
            cfg = mmcv.Config.fromfile(cfg_file)
            cfg.model.pretrained = None

            (checkpoint_file,) = tmpdir.glob("*.pth")
            model = get_inference_model(cfg, str(checkpoint_file), conf_threshold)

        return cls(model)

