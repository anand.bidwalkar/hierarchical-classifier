# partial_labelling

Train a two stage object detector, classifier for tackling partial labelling.

## Requirements

  - GPU machine (Cuda 10.2 capable)
  - Python3.7+
  - Poetry
  - Credentials (see `.env`)

## Setup

```console
source .env
make init
⌛

# open a poetry shell
poetry shell
```

# Poetry Shell

All the commands in this README assume you're in a shell within the poetry venv
created. ie: run `poetry shell` to enter a new shell

## DVC Pipeline

Fill in dvc.yaml values. It's nicely commented for you :-)

```
source .env
export CUDA_VISIBLE_DEVICES=<gpu number>
# Show Pipeline dag
dvc dag

# Show rendered template
dvc render

# To repro a full pipeline
dvc repro

# To repro a single stage
dvc repro -s <stage-name>

# To start a pipeline from a stage and run all stages downstream
dvc repro --downstream <stage-name>

# More dvc commands
dvc repro --help
```

# Inspect Tensorboard

```
# To see training tensorboard
make tb_train

# To see evaluation tensorboard
make tb_eval
```

# Checkin

```
source .env
git add ...
git commit # Fill in template
highlighter log commit
```

# Deployment

**This assumes:**
  - An existing cluster you can deploy to already exists
  - You have a pushed a training run to Highlighter using to steps outlined in
    [Checkin](#checkin)
  - You have created a model or you have a compatiable model's id from the
    Highlighter admin section

```
cd deployment

# Help String
hlops deploy-model --help
```
