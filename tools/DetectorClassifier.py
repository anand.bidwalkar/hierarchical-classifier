import os
from mmpond.model_actions import infer
from mmpond.utils.model_actions import load_config
import json
import cv2
import random
import torch
import classilvier
import gin
import numpy as np
import torch.nn.functional as F
import torchvision.ops.boxes as bops
from highlighter_client.datasets import Dataset
from highlighter_client.datasets import get_reader, get_writer
import pandas as pd
from abc import abstractmethod
import shutil
from pathlib import Path
from tqdm import tqdm

class DetectorClassifierHelper:
	OVERLAYS = False		#Produce images if bboxes superimposed
	THRESHOLD = 0.3			# Detector threshold for prediction

	def __init__(
					self,
					project_name,					
					project_root_dir,
					images_dir,
					output_dir,
					detector_cnf_file,
					detector_chkpt_file,
					detector_annot_file,
					clf_gin_conf_file,
					clf_chkpt_file,
					clf_crop_size,
					num_images
				):

		self.project_name = project_name
		self.project_root_dir = project_root_dir
		self.images_dir = images_dir
		self.output_dir = output_dir

		self.detector_cnf_file = detector_cnf_file
		self.detector_chkpt_file = detector_chkpt_file
		self.detector_annot_file = detector_annot_file

		self.clf_gin_conf_file = clf_gin_conf_file
		self.clf_chkpt_file = clf_chkpt_file
		self.clf_crop_size = clf_crop_size

		self.num_images = num_images
		self.__initialize_process__()


	def __initialize_process__(self):
		"""
		Perform various process intialization steps
		"""
		self.tmp_image_txt_file = '/tmp/test.txt'					# temp file to hold list of images to bulk predict by detector
		self.__load_config__()
		self.__initialize_classifier__()
		self.__read_annotations__()
		self.set_images_list()
		self.__create_tmp_image_file__()

		self.eval_df = pd.DataFrame(columns=['class','tp','fp','fn','precision','recall'])
		self.eval_df_out = os.path.join(self.project_root_dir,'./eval_metrices.tsv')


	def __read_annotations__(self):
		"""
		Convert annotation json to images and annotation dataframes
		"""
		coco_ds = Dataset()
		reader = get_reader("coco")
		coco_ds.reader = reader()
		self.images_df, self.annot_df = coco_ds.read(annotations_file=self.detector_annot_file)
		self.annot_df['image_id'] = self.annot_df['image_id'].astype(str)

	def __clf_get_labels__(self, training_api):
		"""
		Get list of classifier labels
		"""
		self.clf_labels = training_api.class_names


	def __load_config__(self):
		"""
		Load detector config for inference
		"""
		self.cfg = load_config(self.detector_cnf_file)


	def __initialize_classifier__(self):
		"""
		Load classifier from checkpoint
		"""
		gin.parse_config_file(self.clf_gin_conf_file)
		training_api = classilvier.SupervisedClassifier.load_from_checkpoint(self.clf_chkpt_file)
		self.clf_model = training_api._model
		self.__clf_get_labels__(training_api)
		gin.clear_config()
		self.box_classifier = classilvier.BoxClassifier(
			self.clf_model, size=self.clf_crop_size, interpolation='nearest'
		)

	def __get_classifier_prediction__(self, img, bboxes_list):
		"""
		Make classifier prediction given image and list of points (x,y), (w,h)
		"""
		pred_clf_label_list = []
		pred_clf_conf_list = []
		if len(bboxes_list)>0:
			image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
			image = np.moveaxis(image, -1, 0)					# converting to channel first
			image = torch.from_numpy(image).float()
			boxes = torch.tensor(bboxes_list)				# [x,y ,w,h]
			y_logits = self.box_classifier(image[None], boxes) # there are logits
			# converting logits to probability distribution
			y=F.softmax(y_logits, dim=1)
			for each_pred in y:
				label = self.clf_labels[torch.argmax(each_pred)]
				pred_clf_label_list.append(label)
				conf = torch.max(each_pred).item()
				pred_clf_conf_list.append(conf)
		return pred_clf_label_list, pred_clf_conf_list


	def set_images_list(self):
		"""
		Assign list of images to predict
		"""
		if type(self.num_images) == int:
			image_list = self.images_df['id'].tolist()[:self.num_images]
		else:
			image_list = self.images_df['id'].tolist()

		image_name_list = self.images_df[self.images_df['id'].isin(image_list)]['image_filename'].tolist()
		self.image_list = list(map(lambda img_path: os.path.join(self.images_dir, img_path),image_name_list))
		random.shuffle(self.image_list)


	def __create_tmp_image_file__(self):
		"""
		Creates temp image file to be written for bulk detector prediction
		"""
		img_df = pd.DataFrame(columns=['image'])
		img_df['image'] = self.image_list
		img_df.to_csv(self.tmp_image_txt_file, index=False, header=None)


	def __detector_bulk_prediction__(self):
		"""
		Performs bulk detector prediction given txt file containing list of images
		"""
		infer(self.cfg, self.detector_chkpt_file, self.tmp_image_txt_file, self.output_dir, DetectorClassifierHelper.THRESHOLD, DetectorClassifierHelper.OVERLAYS)		# write images to dir
		
	def __classify_detector_predictions__(self, img, pred_data):
		"""
		Per image detected bboxes classification 
		"""

		detected_bboxes = list(map(lambda pred: list(DetectorClassifierHelper.convert_poly_points_to_xywh(pred['polygon'])), pred_data))
		return self.__get_classifier_prediction__(img, detected_bboxes)


	@staticmethod
	def convert_poly_points_to_xywh(poly_points):
		"""
		Convert list of points of poylgon to (x,y) and (w,h)
		"""
		xx, yy = zip(*poly_points)
		x = min(xx); y = min(yy); w = max(xx); h = max(yy)
		return x,y,w,h


	@staticmethod
	def calculate_iou(bbox1, bbox2):
		"""
		Calculate Interection Over Union for two bounding boxes on an image
		"""
		box1 = torch.tensor([bbox1], dtype=torch.float)
		box2 = torch.tensor([bbox2], dtype=torch.float)
		iou = bops.box_iou(box1, box2)
		return iou.item()


	@abstractmethod
	def process(self):
		pass


	def __clean_resources__(self):
		os.remove(self.tmp_image_txt_file)




class DetectorClassifierPredictor(DetectorClassifierHelper):
	"""
	Make two-stage detector and classifier prediction
	"""
	def __init__(
					self,
					project_name,
					project_root_dir,
					images_dir,
					output_dir,
					detector_cnf_file,
					detector_chkpt_file,
					detector_annot_file,
					clf_gin_conf_file,
					clf_chkpt_file,
					clf_crop_size,
					num_images
				):

		super().__init__(			
							project_name,
							project_root_dir,
							images_dir,
							output_dir,
							detector_cnf_file,
							detector_chkpt_file,
							detector_annot_file,
							clf_gin_conf_file,
							clf_chkpt_file,
							clf_crop_size,
							num_images
						)


	def __apply_ground_truth__(self, img, image_prefix):
		"""
		Apply ground truth bboxes and labels on given image
		"""
		single_img_annot_df = self.annot_df[self.annot_df['image_id']==str(image_prefix)]
	
		def draw_gt_on_image(row):
			img_annot = row['location_array']
			x,y,w,h = DetectorClassifierPredictor.convert_poly_points_to_xywh(img_annot)
			gt_label =  row['class_name']
			cv2.rectangle(img,(x,y),(w,h),(0,255,0),2)
			cv2.putText(img, 'gt_{}'.format(gt_label), (x+20,y-80), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,255,0),1)

		single_img_annot_df['location_array'] = single_img_annot_df['location_array'].apply(lambda x: tuple(x))
		single_img_annot_df.drop_duplicates(subset=['image_id','location_array'], keep='first', inplace=True)
		single_img_annot_df.apply(draw_gt_on_image, axis=1)


	def __process_detector_predictions_then_classify__(self, img, image_prefix):
		"""
		Apply prediction bboxes, labels and confidence scores on the image
		"""
		img_det_pred_path = os.path.join(self.output_dir,'{}_prediction.json'.format(image_prefix))
		# handling prediction data
		pred_data = json.load(open(img_det_pred_path))
		
		# Classifying the detected bounding boxes
		pred_clf_label_list, pred_clf_conf_list = self.__classify_detector_predictions__(img, pred_data)

		for pred_idx,pred in enumerate(pred_data):
			# For each bbox predicted
			pred_bbox = pred['polygon']
			pred_label = pred['class']
			pred_conf = pred['confidence']
			x,y,w,h = DetectorClassifierPredictor.convert_poly_points_to_xywh(pred_bbox)
			# For detector
			cv2.rectangle(img,(x,y),(w,h),(0,0,255),2)
			# cv2.putText(img, 'det_{}'.format(pred_label), (x-10,y-40), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,0,0),1)
			# cv2.putText(img, 'det_{:.2f}'.format(pred_conf), (x,y), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,0,0),1)

			# For classifier
			clf_label = pred_clf_label_list[pred_idx]
			clf_conf = pred_clf_conf_list[pred_idx]
			cv2.putText(img, 'clf_{}'.format(clf_label), (x,y+40), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,0,255),1)
			# cv2.putText(img, 'clf_{:.2f}'.format(clf_conf), (x,y+80), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,0,255),1)
		cv2.imwrite(os.path.join(self.output_dir, 'pred_{}.jpg'.format(image_prefix)),img)


	def process(self):
		self.__detector_bulk_prediction__()
		for image in tqdm(self.image_list):
			image_to_infer =  os.path.join(self.images_dir, image)
			image_prefix = image.split('/')[-1].split('.')[0]
			img = cv2.imread(image_to_infer)
			self.__apply_ground_truth__(img, image_prefix)
			self.__process_detector_predictions_then_classify__(img, image_prefix)

		self.__clean_resources__()


	def __clean_resources__(self):
		super().__clean_resources__()
		json_files = list(Path(self.output_dir).glob('*json'))
		for json_file in json_files:
			os.remove(json_file)



class DetectorClassifierEvaluator(DetectorClassifierHelper):
	"""
	Make two-stage detector and classifier prediction evaluation 
	"""
	def __init__(
					self,
					project_name,
					project_root_dir,
					images_dir,
					detector_cnf_file,
					detector_chkpt_file,
					detector_annot_file,
					clf_gin_conf_file,
					clf_chkpt_file,
					clf_crop_size,
					num_images,
					iou_threshold,
					debug = False
				):
		"""
		Use debug mode to save images predictions and their ground truths for emperical analysis.
		"""
		output_dir = os.path.join(project_root_dir, 'temp')
		super().__init__(			
							project_name,
							project_root_dir,
							images_dir,
							output_dir,
							detector_cnf_file,
							detector_chkpt_file,
							detector_annot_file,
							clf_gin_conf_file,
							clf_chkpt_file,
							clf_crop_size,
							num_images
						)
		self.iou_threshold = iou_threshold
		self.debug = debug


	def __initialize_process__(self):
		super().__initialize_process__()
		self.__initialize_eval_process__()


	def __initialize_eval_process__(self):
		"""
		Instantiate evaluation metrices
		"""
		self.tp=0
		self.fp=0
		self.fn=0
		self.gts=0

		self.per_class_eval_mets = {}		# {'cl1':{'tp':float, 'fp':float, 'fn':float}, 'cl2':{'tp':float, 'fp':float, 'fn':float}}


	def __check_label_in_eval_m_dict__(self, label):
		"""
		Check if label is present in eval metrics dict. If not present intiate sub-dict
		"""
		if label not in self.per_class_eval_mets:
			self.per_class_eval_mets[label] = {'tp':0 , 'fn':0, 'fp':0}



	def __evaluate_image__(self, img, image_prefix):
		"""
		Evaluation of image predictions with ground truths for given image
		"""
		# print(image_prefix)
		single_img_annot_df = self.annot_df[self.annot_df['image_id']==str(image_prefix)]
		img_det_pred_path = os.path.join(self.output_dir,'{}_prediction.json'.format(image_prefix))
		# handling prediction data
		pred_data = json.load(open(img_det_pred_path, 'r'))
		
		pred_clf_label_list, pred_clf_conf_list = self.__classify_detector_predictions__(img, pred_data)

		is_image_first_gt = True 		# Boolean flag to append prediction classification labels only for first ground truth iteration

		img_tp = 0
		img_total_preds = len(pred_data)

		def eval_gt_on_image(row):
			# For each gt on image, we compare with each prediction 
			nonlocal pred_data, img_tp, is_image_first_gt, pred_clf_label_list, pred_clf_conf_list
			gt_img_annot = row['location_array']
			gt_x,gt_y,gt_w,gt_h = DetectorClassifierHelper.convert_poly_points_to_xywh(gt_img_annot)
			gt_label =  row['class_name']
			gt_max_iou=0
			gt_max_idx=None

			self.__check_label_in_eval_m_dict__(gt_label)
			# print(gt_img_annot)
			
			if self.debug:
				# Draw gt
				cv2.rectangle(img,(gt_x,gt_y),(gt_w,gt_h),(0,255,0),2)
				cv2.putText(img, 'gt_{}'.format(gt_label), (gt_x,gt_y+160), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,255,0),2)	# All detector predictions are generic objects
		
			for pred_idx,pred in enumerate(pred_data):
				# For each bbox predicted
				pred_bbox = pred['polygon']
				pred_label = pred['class']
				pred_conf = pred['confidence']
				x,y,w,h = DetectorClassifierHelper.convert_poly_points_to_xywh(pred_bbox) 
				img_iou = DetectorClassifierHelper.calculate_iou([gt_x,gt_y,gt_w,gt_h],[x,y,w,h])
				# print(pred_bbox)

				clf_label = pred_clf_label_list[pred_idx]
				
				if is_image_first_gt:
					# Only For 1st instance of ground truth iteration, perform the following processes to avoid redundant computations
					if self.debug:
						clf_conf = pred_clf_conf_list[pred_idx]
						cv2.rectangle(img,(x,y),(w,h),(0,0,255),2)
						# cv2.putText(img, 'det_{}'.format(pred_label), (x-10,y-40), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,0,0),1)
						# cv2.putText(img, 'det_{:.2f}'.format(pred_conf), (x,y), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,0,0),1)
						cv2.putText(img, 'clf_{}'.format(clf_label), (x,y+40), cv2.FONT_HERSHEY_COMPLEX,0.8,(0,0,255),1)
						# cv2.putText(img, 'clf_{:.2f}'.format(clf_conf), (x,y+80), cv2.FONT_HERSHEY_COMPLEX,0.4,(0,0,255),1)

				self.__check_label_in_eval_m_dict__(clf_label)
				
				# TP conditions
				if img_iou>=self.iou_threshold:
					if gt_label==clf_label:
						if img_iou>=gt_max_iou:
							gt_max_iou = img_iou
							gt_max_idx = pred_idx

				# print('------------')

			if gt_max_idx is not None:
				# True Positive for single ground truth
				img_tp += 1
				# removing prediction with matched ground truth for reducing computation for next iterations 
				del pred_data[gt_max_idx]
				del pred_clf_label_list[gt_max_idx]
				del pred_clf_conf_list[gt_max_idx]
				self.per_class_eval_mets[gt_label]['tp'] += 1
			else:
				# False Negative
				self.per_class_eval_mets[gt_label]['fn'] += 1

			is_image_first_gt = False	

		single_img_annot_df['location_array'] = single_img_annot_df['location_array'].apply(lambda x: tuple(x))
		single_img_annot_df.drop_duplicates(subset=['image_id','location_array'], keep='first', inplace=True)
		single_img_annot_df.apply(eval_gt_on_image, axis=1)
		# Eval metrices for entire eval dataset
		self.tp += img_tp
		self.fn += len(single_img_annot_df) - img_tp
		self.fp += img_total_preds - img_tp
		
		# False positives for remaining unmatched predictions
		for rem_pred_clf in pred_clf_label_list:
			self.__check_label_in_eval_m_dict__(rem_pred_clf)
			self.per_class_eval_mets[rem_pred_clf]['fp'] += 1 	

		if self.debug:
			cv2.imwrite(os.path.join(self.output_dir, 'pred_{}.jpg'.format(image_prefix)),img)


	def __print_overall_eval_metrices__(self):
		"""
		Display evaluation metrices
		"""
		print('############################################')
		print('Overall Statistics')
		print('True Positives: {}'.format(self.tp))
		print('False Positives: {}'.format(self.fp))
		print('False Negatives: {}'.format(self.fn))
		self.precision, self.recall = self.__compute_precission_recall__(self.tp, self.fp, self.fn)
		print('Precision: {}'.format(self.precision))
		print('Recall: {}'.format(self.recall))
		print('############################################')
		self.eval_df = self.eval_df.append({
											'class': 'Overall',
											'tp': self.tp,
											'fp': self.fp,
											'fn': self.fn,
											'precision': self.precision, 
											'recall': self.recall
											}, ignore_index=True)

	def __print_per_class_eval_metrices__(self):
		"""
		Display evaluation metrices for each class label
		"""
		print('############################################')
		for label in self.per_class_eval_mets:
			print('------------------')
			print('For class: {}'.format(label))
			print('------------------')
			print('True Positives: {}'.format(self.per_class_eval_mets[label]['tp']))
			print('False Positives: {}'.format(self.per_class_eval_mets[label]['fp']))
			print('False Negatives: {}'.format(self.per_class_eval_mets[label]['fn']))
			class_precision, class_recall = self.__compute_precission_recall__(
																				self.per_class_eval_mets[label]['tp'],
																				self.per_class_eval_mets[label]['fp'],
																				self.per_class_eval_mets[label]['fn']
																			   )

			print('Precision: {}'.format(class_precision))
			print('Recall: {}'.format(class_recall))
			self.eval_df = self.eval_df.append({
												'class': label,
												'tp': self.per_class_eval_mets[label]['tp'],
												'fp': self.per_class_eval_mets[label]['fp'],
												'fn': self.per_class_eval_mets[label]['fn'],
												'precision': class_precision, 
												'recall': class_recall
												}, ignore_index=True)
		print('############################################')


	def __compute_precission_recall__(self, tp, fp, fn):
		"""
		Computes precision and recall given true positives, false positives and false negatives
		"""
		denom = tp+fp
		if denom==0:
			denom = 1
		precision = tp/(denom)
		denom = tp+fn
		if denom==0:
			denom = 1
		recall = tp/(denom)
		return precision, recall


	def __perform_evaluation__(self):
		self.__print_per_class_eval_metrices__()
		self.__print_overall_eval_metrices__()
		self.eval_df.to_csv(self.eval_df_out, index=False, sep='\t')


	def process(self):
		self.__detector_bulk_prediction__()
		for image in tqdm(self.image_list):
			image_to_infer =  os.path.join(self.images_dir, image)
			image_prefix = image.split('/')[-1].split('.')[0]
			img = cv2.imread(image_to_infer)
			self.__evaluate_image__(img, image_prefix)

		self.__perform_evaluation__()
		self.__clean_resources__()


	def __clean_resources__(self):
		super().__clean_resources__()
		if self.debug:
			json_files = list(Path(self.output_dir).glob('*json'))
			for json_file in json_files:
				os.remove(json_file)
		# else:
		# 	shutil.rmtree(self.output_dir)
