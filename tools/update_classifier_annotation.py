import helper
import click
import yaml

@click.command()
@click.option("--annotation_file", required=True, help="Path to your detector annotations.")
@click.option("--class_mapper_file", required=True, help="Yaml file containing class name mapping")
@click.option("--project_name", required=True, help="Project Name.")
@click.option("--output_ann_dir", required=True, help="Output path where the updated annotation data is stored")
@click.option("--output_images_dir", required=True, help="Output path where the updated annotation data is stored")

def update_classifier_annotation(annotation_file, class_mapper_file, project_name, output_ann_dir, output_images_dir):    
    
    if class_mapper_file and ".yaml" in class_mapper_file:
        patterns, new_names = [], []
        with open(class_mapper_file, "r") as f:
            parsed_yaml_file = yaml.safe_load(f)
        if "class_mapper" in parsed_yaml_file:
            patterns.extend(parsed_yaml_file["class_mapper"].keys())
            new_names.extend(parsed_yaml_file["class_mapper"].values())

        if len(patterns) != len(new_names):
            raise ValueError(
                (
                    f"Each old name in '{patterns}' must have a new name. "
                    f"Got new_names: '{new_names}'"
                )
            )

        coco_ds = helper.get_coco_dataset_object(annotation_file)
        helper.create_dir(output_ann_dir)
        

        for pattern, n_name in zip(patterns, new_names):
            coco_ds.annotations_df['class_name'] = coco_ds.annotations_df['class_name'].str.replace(pattern, n_name)
        coco_ds.annotations_df.to_json('/home/anand.bidwalkar/workspace/xxasdas.json')
    #class_image_cnt = coco_ds.annotations_df.groupby(['class_name'],as_index=False)['image_id'].count()
    #class_image_cnt = class_image_cnt.sort_values(by=['image_id'], ascending=False)
    #classes_to_update = class_image_cnt.iloc[-update_class_cnt:]['class_name'].tolist()              # picking out the last n classes
    #coco_ds.annotations_df.loc[coco_ds.annotations_df['class_name'].isin(classes_to_update), 'class_name'] = generic_object_name 
        helper.write_coco_dataset_to_disk(coco_ds, output_ann_dir, output_images_dir)

if __name__ == '__main__':
    update_classifier_annotation()
