import click
import os
import ast 
from pathlib import Path
from highlighter_client.datasets.export import ExportDataset
from highlighter_client import graphql as gql
from highlighter_client.datasets import data_snapshots

DOWNLOAD_THREADS = 5


@click.command()
@click.option("--data_dir", required=True, help="root path where the dataset is downloaded")
@click.option("--project_id_list", required=True, help="List of project ids")
@click.option("--hl_hash_dict", required=False, help="list of hashes")
@click.option("--human_annot_id_list", required=False, help="List of human annotated project ids")
@click.option("--data_format", required=False, help="Format of data to be downloaded")
@click.option("--splits", required=False, help="Splits ratio")
@click.option("--seed", required=True, help="Random Seed", type=int, default=42)
def download_multi_datasets(	
								data_dir, 
								project_id_list, 
								hl_hash_dict, 
								human_annot_id_list, 
								data_format,
								seed,
								splits='{"train":1,"eval":0}'
							):
	"""
	Downloads multiple dataset provided list of project_id and/or hashes
	"""
	download_threads = 10
	project_id_list = ast.literal_eval(project_id_list) 
	hl_hash_dict = ast.literal_eval(hl_hash_dict) 
	human_annot_id_list = ast.literal_eval(human_annot_id_list) 
	splits = ast.literal_eval(splits)

	project_id_list = set(project_id_list) -set(human_annot_id_list)

	with gql.HighlighterContext():
		ep = ExportDataset('WPS')
		# For all annotations
		for proj_id in project_id_list:
			output_dir = os.path.join(data_dir, str(proj_id))
			output = Path(output_dir)
			ep.set_filter(
				project_id=[proj_id]
			)
			ep.write(
				output,
				data_format,   
				splits=splits,
				seed=seed,
				download_threads=DOWNLOAD_THREADS,
			)

		# # For human annotations
		for proj_id in human_annot_id_list:
			output_dir = os.path.join(data_dir, str(proj_id))
			output = Path(output_dir)
			ep.set_filter(
				project_id=[proj_id],
				annotation_attribute='MANUAL_ANNOTATIONS'
			)
			ep.write(
				output,
				data_format,
				splits=splits,
				seed=seed,
				download_threads=DOWNLOAD_THREADS,
			)

		# For hashes
		for proj_id in hl_hash_dict:
			proj_hash = hl_hash_dict[proj_id]
			dataset = data_snapshots.export_snapshot(proj_hash, data_format)
			output_dir = os.path.join(data_dir, str(proj_id))
			dataset.write(data_root=output_dir, download_threads=DOWNLOAD_THREADS, shared=True)

if __name__ == '__main__':
	download_multi_datasets()
