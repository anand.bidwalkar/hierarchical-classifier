import json
import os
import click
import random

def create_dir(dir_path):
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)


def get_train_eval_split_idxs(num_images, rand_seed, eval_split=0 ):
    """
    Returns random tranning and evaluation indices from entire dataset.

    Params:
        num_images: Number of images to be split.
        rand_seed: Random seed for reproducibility of randomization.
        eval_split: Percentage of evaluation dataset.
    """
    img_idx_list = list(range(0, num_images))
    # shuffle the list of image ids
    random.Random(rand_seed).shuffle(img_idx_list)
    num_eval_images = int(eval_split*num_images)    
    return set(img_idx_list[num_eval_images:]), set(img_idx_list[:num_eval_images])



@click.command()
@click.option("--annotation_file", required=True, help="Downloaded highlighter annotations file.")
@click.option("--project_name", required=True, help="Project Name.")
@click.option("--output_dir", required=True, help="Output path where the train/eval sannotation split data is saved.")
@click.option("--split", required=True, help="Train evaluation dataset split ratio")
@click.option("--seed", required=True, help="Random seed for reproducebility.")
def split_detector_annotation(annotation_file, project_name, output_dir, split, seed):
    """
    Split the downloaded highlighter annotation to custom dectector train and evaluation annotations for detector stage.
    """
    create_dir(output_dir)
    split = eval(split)
    with open(annotation_file, 'r') as in_annot:
        in_annot_js = json.load(in_annot)

        num_images = len(in_annot_js['images'])
        train_idxs, eval_idxs = get_train_eval_split_idxs(num_images, seed, split['eval'])
        train_annot = {
                        'images': [],
                        'annotations': [],
                        'categories': []
                        }
        eval_annot = {
                        'images': [],
                        'annotations': [],
                        'categories': []
                        }

        # Placeholders to hold lookup values
        train_imgs_set = set()
        train_cat_set = set()

        eval_imgs_set = set()
        eval_cat_set = set()

        # splitting images
        for idx,in_annot_js_img in enumerate(in_annot_js['images']):
            if idx in train_idxs:
                in_annot_js_img['split'] = 'train'
                train_annot['images'].append(in_annot_js_img)
                train_imgs_set.add(in_annot_js_img['id'])
            else:
                in_annot_js_img['split'] = 'eval'
                eval_annot['images'].append(in_annot_js_img)
                eval_imgs_set.add(in_annot_js_img['id'])


        # splitting annotations
        for in_annot_js_annot in in_annot_js['annotations']:
            img_id = in_annot_js_annot['image_id']
            img_cat_id = in_annot_js_annot['category_id']
            if img_id in train_imgs_set:
                train_annot['annotations'].append(in_annot_js_annot)
                train_cat_set.add(img_cat_id)
            else:
                eval_annot['annotations'].append(in_annot_js_annot)
                eval_cat_set.add(img_cat_id)


        # splitting categories
        for in_annot_js_cat in in_annot_js['categories']:
            cat_id = in_annot_js_cat['id']
            if cat_id in train_cat_set:
                train_annot['categories'].append(in_annot_js_cat)
            if cat_id in eval_cat_set:
                eval_annot['categories'].append(in_annot_js_cat)

        dectector_train_annot_path = os.path.join(output_dir, '{}_detector_train.json'.format(project_name))
        dectector_eval_annot_path = os.path.join(output_dir, '{}_detector_eval.json'.format(project_name))

        with open(dectector_train_annot_path, 'w') as train_annot_f:
            json.dump(train_annot, train_annot_f)

        with open(dectector_eval_annot_path, 'w') as eval_annot_f:
            json.dump(eval_annot, eval_annot_f)


if __name__ == '__main__':
    split_detector_annotation()
