import pandas as pd
from pathlib import Path
from tbreader import SummaryReader
import click
import json


@click.group()
def main():
    pass


def load_logs(logdir):
    files = list(Path(logdir).glob('*.tfevents.*'))
    df = pd.DataFrame()
    for path in files:
        # TODO Change to tensorboardx parser or custom implementation
        for summary in SummaryReader(str(path)):
            if len(summary.summary.value) == 0: continue
            assert len(summary.summary.value) == 1
            step = summary.step
            summary, = summary.summary.value
            key = summary.tag
            try:
                value = summary.simple_value
            except AttributeError:
                print(f'Nope for {key}')
            else:
                df.loc[step, key] = value
    df = df.fillna(0)
    return df


@click.command('best-checkpoint')
@click.option('--logdir', '-i',
        type=click.Path(file_okay=False, exists=True),
        required=True,
        help="Path tp Tensorboard logs")
def best_checkpoint(logdir):
    df = load_logs(logdir)
    idx = df['bbox_mAP_50'].idxmax()
    print(f'epoch_{idx}.pth')


@click.command('parse')
@click.option('--logdir', '-i',
        type=click.Path(file_okay=False, exists=True),
        required=True,
        help="Path tp Tensorboard logs")
@click.option('--metrics-file', '-o',
        type=click.Path(dir_okay=False, writable=True), required=True,
        help="Output file path")
@click.option('--metrics', '-m', type=str, multiple=True,
        help=("<tensorboard-metric-name>:<highlighter-metric-name>:<min | max"
            " depending on what the 'best' metric is>"))
def parse_tensorboard(logdir, metrics_file, metrics):
    df = load_logs(logdir)
    vals = dict()
    for metric in metrics:
        tb_name, hl_name, agg = metric.split(':')
        if tb_name not in df.columns:
            print((f"WARNING: Could not locate column '{tb_name}' "
                    "in tensorboard. Skipping"))
            # Stupid graphql does no allow us to push metrics
            # with no values so we just make this -1 to
            # indicate something is amiss
            vals[hl_name] = -1
            continue

        assert agg in {'max', 'min'}
        value = getattr(df[tb_name], agg)()


        # Stupid graphql parses 0.0 as Null so we just make
        # all 0.0s very small
        if value == 0.0: value = 1e-7
        vals[hl_name] = value

    with open(metrics_file, 'w') as buf:
        json.dump(vals, buf)


main.add_command(best_checkpoint)
main.add_command(parse_tensorboard)


if __name__ == '__main__':
    main()
