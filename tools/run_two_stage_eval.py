import click
import os
from DetectorClassifier import DetectorClassifierEvaluator
from pathlib import Path



def get_classifier_checkpoint(clf_chkpt_path):
	chkpt_files = list(Path(clf_chkpt_path).glob('*ckpt'))
	return os.path.join(clf_chkpt_path, chkpt_files[0])



@click.command()
@click.option("--project_name", required=True, help="name of project")
@click.option("--project_root_dir", required=True, help="root dir of project process")
@click.option("--images_dir", required=True, help="Directory of images")
@click.option("--detector_conf_file", required=True, help="File path of detector config")
@click.option("--detector_chkpt_file", required=True, help="File path to detector checkpoint")
@click.option("--eval_annot_path", required=True, help="File path to annotations to evaluate model performance")
@click.option("--clf_gin_config", required=True, help="File path to gin config")
@click.option("--clf_chkpt_path", required=True, help="Dir path of classifier checkpoint")
@click.option("--num_images", required=False, help="Number of Images to evaluate", default='all')
@click.option("--iou_threshold", required=False, help="IoU threshold to consider prediction as True Positive", default=0.5)
def two_stage_evaluate(	
						project_name,
						project_root_dir,
						images_dir,
						detector_conf_file,
						detector_chkpt_file,
						eval_annot_path,
						clf_gin_config,
						clf_chkpt_path,
						num_images,
						iou_threshold
				   ):
	"""
	Run two-stage eval
	"""
	clf_chkpt_file = get_classifier_checkpoint(clf_chkpt_path)
	try:
		num_images = int(num_images)
	except:
		pass
	det_clf_eval = DetectorClassifierEvaluator(
													project_name,
													project_root_dir,
													images_dir,
													detector_conf_file,
													detector_chkpt_file,
													eval_annot_path,
													clf_gin_config,
													clf_chkpt_file,
													(128, 128),
													num_images,
													iou_threshold,
													True
												 )
	det_clf_eval.process()

if __name__ == '__main__':
	two_stage_evaluate()
