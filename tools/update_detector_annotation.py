import json
import helper
from pathlib import Path
import click
import os

@click.command()
@click.option("--annotation_dir", required=True, help="Path to your detector annotations.")
@click.option("--generic_object_name", required=False, default='object', help="Class name to replace all annotation classes for the object detector.")
@click.option("--project_name", required=True, help="Project Name.")
@click.option("--output_dir", required=True, help="Output path where the updated annotation data is stored")
def update_detector_annotation(annotation_dir, generic_object_name, project_name, output_dir):
    files = list(Path(annotation_dir).glob('*json'))
    helper.create_dir(output_dir)
    for file_path in files:
        data = json.load(open(file_path, 'r'))
        # creating generic object class
        data['categories'] = [{'id':1,'name':generic_object_name}]
        # updating each annotation class
        for idx,annot in enumerate(data['annotations']):
            data['annotations'][idx]['category_id'] = 1
        output_file_path = os.path.join(
                                        output_dir,
                                        file_path.name.replace(project_name, generic_object_name)
                                        )
        with open(output_file_path, 'w') as outfile:
            json.dump(data, outfile)


if __name__ == '__main__':
    update_detector_annotation()
