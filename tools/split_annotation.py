import os
import click


@click.command()
@click.option("--yaml_file", required=True, help="Yaml configuration file for detection")
@click.option("--module_path", required=True, help="Path to python train-test module")
@click.option("--images_dir", required=True, help="Path to images of annotations")
def split_annotation(yaml_file, module_path, images_dir):
    """
    Split the downloaded highlighter annotation to custom dectector train and evaluation annotations.
    """
    # adding python module path prior to import
    import sys
    sys.path.append(module_path)
    from dataset_splits.splitter.detector.coco import CocoDatasetSplitter

    coco_ds = CocoDatasetSplitter(yaml_file)
    coco_ds.read_annot_from_disk()
    coco_ds.perform_split()
    coco_ds.ds.images_dir = images_dir
    coco_ds.write_annot_to_disk()


if __name__ == '__main__':
    split_annotation()
