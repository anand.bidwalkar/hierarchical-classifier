import click
import os
import ast
import helper
from pathlib import Path
from shutil import copy
from merge_dataset import *


ANTELOPE_SUB_SPECIES_TO_UPDATE = ['alcelaphus buselaphus','cephalophus harveyi','cephalophus nigrifrons','eudorcas thomsonii','kobus ellipsiprymnus',\
								'macaca arctoides','madoqua guentheri','nanger granti','nesotragus moschatus','oreotragus oreotragus','tragelaphus strepsiceros',\
								'philantomba monticola','raphicerus campestris','sylvicapra grimmia','tragelaphus oryx','tragelaphus scriptus', 'litocranius walleri']



def get_unique_annotation_ids(df):
	"""
	For annotations with duplicate annotation id due to genus and species duplication
	"""
	annot_id_max = df['annotation_id'].max()

	def update_annotation_id(dup_annot_id):
		nonlocal annot_id_max
		annot_id_max += 1
		return annot_id_max

	df.update(

				df.loc[
						df['annotation_id'].duplicated(),
						'annotation_id'
					 ].apply(update_annotation_id)
			)
	return df

@click.command()
@click.option("--data_dir", required=True, help="root path where the datasets are downloaded")
@click.option("--project_merge_list", required=True, help="string representation of list of projects to merge")
@click.option("--project_unknown_merge_list", required=True, help="string representation of list of projects to merge with the prior list that contains the unknown classes")
@click.option("--class_filter_dict", required=False, help="string representation of dictionary containing list of classes to filter for project")
@click.option("--update_class_names_dict", required=False, help="string representation of dictionary containing old class name as key with updated class name as value.")
@click.option("--output_annot_dir", required=True, help="output path where the annotations are saved")
@click.option("--output_images_dir", required=True, help="output path where the images ares saved")
def main(	
						data_dir,
						project_merge_list,
						project_unknown_merge_list,
						class_filter_dict,
						update_class_names_dict,
						output_annot_dir,
						output_images_dir
				   ):
	"""
	Downloads multiple dataset provided list of project_id and/or hashes
	"""
	class_filter_dict = ast.literal_eval(class_filter_dict)
	update_class_names_dict = ast.literal_eval(update_class_names_dict)
	project_merge_list = ast.literal_eval(project_merge_list)
	project_merge_list = list(map(lambda x: str(x), project_merge_list))
	project_unknown_merge_list = ast.literal_eval(project_unknown_merge_list)
	project_unknown_merge_list = list(map(lambda x: str(x), project_unknown_merge_list))

	print('Merging projects: {}'.format(','.join(project_merge_list)))
	merged_ds = get_merged_dataset(data_dir, project_merge_list, class_filter_dict, update_class_names_dict)
	merged_ds = rename_classes(merged_ds, update_class_names_dict)

	print('Merging Unknown projects: {}'.format(','.join(project_unknown_merge_list)))
	unknown_merged_ds = get_merged_dataset(data_dir, project_unknown_merge_list, class_filter_dict, update_class_names_dict)
	unknown_merged_ds = rename_classes(unknown_merged_ds, update_class_names_dict)

	merged_ds_class_names = merged_ds.annotations_df['class_name'].unique()

	antelope_species_to_update_to_genus = list(set(ANTELOPE_SUB_SPECIES_TO_UPDATE) - set(merged_ds_class_names))

	# convert antelope subspecies to genus Antelope
	unknown_merged_ds.annotations_df.loc[
											unknown_merged_ds.annotations_df['class_name'].isin(
																									antelope_species_to_update_to_genus
																								),
										'class_name'] = 'Antelope'
	
	unknown_merged_ds.annotations_df.loc[~(
											unknown_merged_ds.annotations_df['class_name'].isin(
																									merged_ds_class_names)), 
										'class_name'] = 'Unknown'

	
	merged_ds = merge_datasets(merged_ds, unknown_merged_ds)
	merged_ds = filter_classes(merged_ds, class_filter_dict, 'all')
	
	# Duplicating annotations for subspecies of antelopes
	antelope_sub_species_df = merged_ds.annotations_df[merged_ds.annotations_df['class_name'].isin(ANTELOPE_SUB_SPECIES_TO_UPDATE+['Duiker (genus)','Dik-dik (genus)'])].copy()
	antelope_sub_species_df['class_name'] = 'Antelope'
	
	merged_ds.annotations_df = merged_ds.annotations_df.append(antelope_sub_species_df)
	merged_ds.annotations_df.reset_index(inplace=True)
	del merged_ds.annotations_df['index']
	# for duplicate annotation_ids update id
	merged_ds.annotations_df = get_unique_annotation_ids(merged_ds.annotations_df)

	create_images_copy(merged_ds, data_dir, output_images_dir)
	# writing files to disk
	helper.create_dir(output_annot_dir)
	merged_ds.write(
        images_dir=output_images_dir,
        annotations_dir=output_annot_dir,
        annotations_file_prefix='WPS',
        strip_metadata=True,
    )

if __name__ == '__main__':
	main()
