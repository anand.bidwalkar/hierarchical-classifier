import os
import pandas as pd
from highlighter_client.datasets import Dataset
from highlighter_client.datasets import get_reader, get_writer

def create_dir(dir_path):
	"""
	Create directory if doesn't exists.
	"""
	if not os.path.exists(dir_path):
		os.mkdir(dir_path)


def get_coco_dataset_object(annot_file):
	"""
	Return cocodataset object created based on given annotation details
	"""
	coco_ds = Dataset()
	reader = get_reader("coco")
	coco_ds.reader = reader()
	writer = get_writer("coco")
	coco_ds.writer = writer()
	images_df, annot_df = coco_ds.read(annotations_file=annot_file)
	return coco_ds


def get_coco_merged_annotation_dataframe(annot_file, project_name='test'):
	"""
	Return merged dataframe based on annotation file
	"""
	coco_ds = get_coco_dataset_object(annot_file, project_name)
	images_df, annot_df = coco_ds.read(annotations_file=annot_file)
	images_annot_df = pd.merge(images_df, annotations_df, how='inner', left_on=['id'], right_on=['image_id'])
	return images_annot_df

def write_coco_dataset_to_disk(coco_ds, out_ann_dir, out_image_dir):
	coco_ds.write(annotations_dir=out_ann_dir, images_dir=out_image_dir)
