import click
import helper

@click.command()
@click.option("--annot_file", required=True, help="Path to annotation file")
@click.option("--field", required=True, help="Field used to count the number of instances")
@click.option("--top_n", required=False, help="Number of Top N field instances by count to include", type=int, default=0)
@click.option("--bottom_n", required=False, help="Number of Bottom N field instances by count to include", type=int, default=0)
def get_class_instances_for_heldout_split(annot_file, field, top_n=0, bottom_n=None):
	"""
	"""
	images_annot_df = helper.get_coco_merged_annotation_dataframe(annot_file)
	# Count of images per class
	class_image_cnt = images_annot_df.groupby([field],as_index=False)['image_id'].count()
	class_image_cnt = class_image_cnt.sort_values(by=['image_id'], ascending=False)

	if top_n>0:
		class_image_cnt = class_image_cnt.iloc[top_n:]

	if bottom_n>0:
		class_image_cnt = class_image_cnt.iloc[:-bottom_n]

	heldout_list = class_image_cnt[field].tolist()
	# output to be used as environment variable
	print(heldout_list)


if __name__ == '__main__':
	get_class_instances_for_heldout_split()
