import click
import os
import ast
import helper
from pathlib import Path
from shutil import copy


def get_coco_dataset(project_id, project_dir):
	"""
	Return coco dataset images and annotations dataframes from given annotation file path
	"""
	annot_dir = os.path.join(project_dir, 'annotations')
	annot_path = list(Path(annot_dir).glob('*json'))[0]
	annot_path = os.path.join(annot_dir, annot_path.name)

	coco_ds = helper.get_coco_dataset_object(annot_path)
	coco_ds.images_df['proj_id'] = project_id
	coco_ds.images_df['split'] = 'data'

	# # For test
	# coco_ds.images_df = coco_ds.images_df.iloc[:10]
	# coco_ds.annotations_df = coco_ds.annotations_df[coco_ds.annotations_df['image_id'].isin(coco_ds.images_df['id'])]
	return coco_ds


def filter_classes(ds, filter_dict, proj_name):
	"""
	Filters classes from the list
	"""
	if proj_name in filter_dict:
		ds.annotations_df = ds.annotations_df[~(ds.annotations_df['class_name'].isin(filter_dict[proj_name]))]
	return ds


def merge_datasets(ds_1, ds_2):
	"""
	Merge two datasets into one
	"""
	ds_1.images_df = ds_1.images_df.append(ds_2.images_df) 
	ds_1.annotations_df = ds_1.annotations_df.append(ds_2.annotations_df)
	ds_1.annotations_df.drop_duplicates(subset=['image_id', 'annotation_id'], keep='first', inplace=True)
	ds_1.images_df = ds_1.images_df[ds_1.images_df['id'].isin(ds_1.annotations_df['image_id'])]
	ds_1.images_df.drop_duplicates(subset=['id'], keep='first', inplace=True)

	ds_1.images_df.reset_index(inplace=True)
	del ds_1.images_df['index']
	ds_1.annotations_df.reset_index(inplace=True)
	del ds_1.annotations_df['index']
	return ds_1


def rename_classes(ds, class_names_update):
	"""
	Rename classes where the key is the old class name and the new class name is the value
	"""
	for old_class_val in class_names_update:
		ds.annotations_df['class_name'] = ds.annotations_df['class_name'].str.replace(old_class_val, class_names_update[old_class_val])
	return ds


def create_images_copy(ds, input_dir, output_dir):
	"""
	Create copy of images
	"""
	helper.create_dir(output_dir)
	def create_symlink_for_each_image_id(row):
		nonlocal output_dir
		image_name = row['image_filename']
		proj_id = row['proj_id']
		src_img = os.path.join(input_dir, proj_id,'images', image_name)
		dest_img = os.path.join(output_dir, image_name)
		copy(src_img, dest_img)
	ds.images_df.apply(create_symlink_for_each_image_id, axis=1)
	del ds.images_df['proj_id']



def get_merged_dataset( 
						data_dir,
						project_merge_list,
						class_filter_dict,
						update_class_names_dict
						):
	"""
	Merge multiple list of datasets then filter class instances if exists and update class_names if exists
	"""
	proj_dir = project_merge_list[0]
	merged_ds = get_coco_dataset(proj_dir, os.path.join(data_dir, proj_dir))
	if class_filter_dict:
		merged_ds = filter_classes(merged_ds, class_filter_dict, proj_dir)
	print("Got COCO Dataset")
	for proj_dir in project_merge_list[1:]:
		proj_out_dir = os.path.join(data_dir, proj_dir)
		proj_ds = get_coco_dataset(proj_dir, proj_out_dir)
		proj_ds = filter_classes(proj_ds, class_filter_dict, proj_dir)
		merged_ds = merge_datasets(merged_ds, proj_ds)
	
	if class_filter_dict:
		merged_ds = filter_classes(merged_ds, class_filter_dict, 'all')
	if update_class_names_dict:
		merged_ds = rename_classes(merged_ds, update_class_names_dict)
	print("Merged")
	return merged_ds

@click.command()
@click.option("--data_dir", required=True, help="root path where the datasets are downloaded")
@click.option("--project_merge_list", required=True, help="string representation of list of projects to merge")
@click.option("--class_filter_dict", required=False, help="string representation of dictionary containing list of classes to filter for project")
@click.option("--update_class_names_dict", required=False, help="string representation of dictionary containing old class name as key with updated class name as value.")
@click.option("--output_annot_dir", required=True, help="output path where the annotations are saved")
@click.option("--output_images_dir", required=True, help="output path where the images ares saved")
def main(	
						data_dir,
						project_merge_list,
						class_filter_dict,
						update_class_names_dict,
						output_annot_dir,
						output_images_dir
				   ):
	"""
	Downloads multiple dataset provided list of project_id and/or hashes
	"""
	if class_filter_dict:
		class_filter_dict = ast.literal_eval(class_filter_dict)
	if update_class_names_dict:
		update_class_names_dict = ast.literal_eval(update_class_names_dict) 
	
	project_merge_list = ast.literal_eval(project_merge_list)
	project_merge_list = list(map(lambda x: str(x), project_merge_list))

	print('Merging projects: {}'.format(','.join(project_merge_list)))

	merged_ds = get_merged_dataset(data_dir, project_merge_list, class_filter_dict, update_class_names_dict)
	create_images_copy(merged_ds, data_dir, output_images_dir)
	# writing files to disk
	merged_ds.images_dir = output_images_dir
	merged_ds.dataset_name = 'WPS'
	helper.create_dir(output_annot_dir)
	merged_ds.write(
        images_dir=output_images_dir,
        annotations_dir=output_annot_dir,
        annotations_file_prefix='WPS',
        strip_metadata=True,
    )

if __name__ == '__main__':
	main()
