"""
Plot evaluation metrices for various classes comparing two separate classifier training.

"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
 
# set width of bar
BAR_WIDTH = 0.25


SUFFIX_1 = '15'
SUFFIX_2 = '16'


def update_df(df, suffix):
	for col in df.columns:
		if col =='class':
			continue
		if col in ['precision','recall']:
			df[col] = df[col]*100
		df.rename(columns={col:'{}_{}'.format(col, suffix)}, inplace=True)
	return df


if __name__ == '__main__':
	
	df1 = pd.read_csv('eval_metrices_clf15.tsv', sep='\t')
	df2 = pd.read_csv('eval_metrices_clf16.tsv', sep='\t')

	df1 = update_df(df1, SUFFIX_1)
	df2 = update_df(df2, SUFFIX_2)

	merged_df = pd.merge(df1, df2, how='right', on=['class'])
	merged_df.sort_values(by=['class'], ascending=True, inplace=True)

	overall_row = merged_df[merged_df['class']=='Overall']

	merged_df = merged_df.iloc[1:].append(overall_row)


	merged_df.fillna(value=0,inplace=True)

	num_classes = len(merged_df)
	x_pos = np.arange(num_classes)

	for col in ['tp','fp','fn','precision','recall']:
		print(col)
		columns = ['{}_15'.format(col),'{}_16'.format(col)]
		data = merged_df[columns]
		fig, ax = plt.subplots(figsize=(12, 5), dpi=100)
		ax.bar(x_pos, data.iloc[:, 0],  color='#7f6d5f', width=BAR_WIDTH, edgecolor='white', label=columns[0], align='center')
		ax.bar(x_pos+BAR_WIDTH, data.iloc[:, 1],  color='#557f2d', width=BAR_WIDTH, edgecolor='white', label=columns[1], align='center')
		fig.tight_layout()
		ax.legend()
		ax.set_xlabel("Classifier Trainned classes for clf_15 vs clf_16")
		ax.set_ylabel("{} Evaluation Metric".format(col))
		ax.set_title("Model Performance Comparision for clf_15 vs clf_16")
		ax.set_xticks(x_pos+BAR_WIDTH)
		ax.set_xticklabels(merged_df['class'], rotation=90)
		fig.tight_layout()
		plt.savefig('Eval_{}.png'.format(col))